const express = require("express"),
path = require("path"),
bodyParser = require("body-parser")

const PORT = process.env.PORT || 8080

const staticPath = path.join(__dirname, 'static')

var app = express()

app.use(express.static(staticPath))

// app.use(bodyParser.urlencoded({ extended: true }))

app.listen(PORT, () => {
    console.log("Now listening on PORT: " + PORT)
})

app.get('/mainProx', async (req, res) => {
    var fullURL = req.headers.host
    var url = req.query.url
    var data = await fetch(url)

    var textData = await data.text()

    textData = textData.replaceAll(`href="https://`, `href="https://` + fullURL + "/proxy/8080/mainProx?url=")
    textData = textData.replaceAll(`src="https://"`, `src="https://` + fullURL + "proxy/8080/mainProx?url=")
    textData = textData.replaceAll(`href="/`, `href="https://` + fullURL + "/proxy/8080/mainProx?url=" + url + "/")
    textData = textData.replaceAll(`src="/`, `src="https://` + fullURL + "/proxy/8080/mainProx?url=" + url + "/")
    // data = data.replaceAll(`href="./`, `href=`)

    
    console.log(data.headers)
    res.writeHead(data.headers)
    
    res.send(textData)
})

app.get('/proxResource', async (req, res) => {
    var url = req.query.url
    var data = await (await fetch(url)).text()
    
    res.send(data)
})